##--------------------------------------------------
## Procedure    : get_spare_cells
## Author       : aperdana
## Description  : Return a collection of spare cells based on pattern specified.
##                Will use default pattern *spare* if no pattern specified
##--------------------------------------------------
proc get_spare_cells {{pattern *spare*}} {
    set cells_matching_pattern [get_flat_cells $pattern]
    set spare_cells ""
    foreach_in_collection cell $cells_matching_pattern {
        set out_nets [get_flat_nets -quiet -of_objects [get_flat_pins -of_objects $cell -filter "direction==out"]]
        if {[sizeof_collection $out_nets] == 0} {
            set spare_cells [add_to_collection $spare_cells $cell]
        }
    }
    set num_of_spare_cells [sizeof_collection $spare_cells]
    echo "Number of spare cells matching pattern $pattern = $num_of_spare_cells"
    return $spare_cells
}
