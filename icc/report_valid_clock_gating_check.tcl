##--------------------------------------------------
## Procedure    : is_clock_gating_valid
## Author       : aperdana
## Description  : Intermediate function for report_valid_clock_gating_check procedure.
##                Return 1 if the fanout of cell belongs to pin in argument is connected to clock pins.
##--------------------------------------------------
proc is_clock_gating_valid {endpoint} {
    set endpoint [get_flat_pins $endpoint]
    set driver_pin [get_flat_pins -of_objects [get_flat_cell -of_objects $endpoint] -filter "direction=~out"]
    set load_pin [remove_from_collection [get_flat_pins -of_objects [get_flat_nets -of_objects $driver_pin]] $driver_pin]
    set clock_load_pin [filter_collection $load_pin "is_clock_pin==true and (name=~CK* or name=~G*)"]
    set non_clock_load_pin [remove_from_collection $load_pin $clock_load_pin]
    if {[sizeof_collection $clock_load_pin] > 0} {
        # The output is connected to at least one clock pin
        return 1
    } elseif {[sizeof_collection $clock_load_pin] == 0 && [sizeof_collection $load_pin] == 0} {
        # The output is either connected to output/inout ports or floating
        return 0
    } else {
        # The output is connected to input pins of combinational cells
        # Will check the fanout of those cells
        set is_valid 0
        foreach_in_collection pin $non_clock_load_pin {
            set is_valid [expr $is_valid || [is_clock_gating_valid $pin]]
            #if {$is_valid == 1} {
                #break
            #}
        }
        return $is_valid
    }
} 
##--------------------------------------------------
## Procedure    : report_valid_clock_gating_check
## Author       : aperdana
## Description  : Generate report of valid clock gating check for all active scenarios.
##                Clock gating check valid if the output of endpoint is used to clock sequential cells.
##                Paths checked are violating paths (slack < 0) on ***clock_gating_default*** path group.
##--------------------------------------------------
proc report_valid_clock_gating_check {{group **clock_gating_default**} {max_paths 100}} {
    set false_end ""
    puts "Running report_valid_clock gating_check for $group with maximum # of paths $max_paths...\n"
    suppress_message {ATTR-1 ATTR-3 TIM-104}
    foreach scen [all_active_scenarios] {
        if {[regexp setup $scen]} {
            set clkg_path [get_timing_paths -delay_type max -nworst 1 -max_paths $max_paths -group $group -slack_lesser_than 0 -include_hierarchical_pins -path_type full_clock_expanded -scenarios $scen]
        } else {
            set clkg_path [get_timing_paths -delay_type min -nworst 1 -max_paths $max_paths -group $group -slack_lesser_than 0 -include_hierarchical_pins -path_type full_clock_expanded -scenarios $scen]
        }
        puts "\n===== Scenario: $scen =====" 
        if {[sizeof_collection $clkg_path] > 0} {
            foreach_in_collection path $clkg_path {
                set slack [get_attribute $path slack]
                set start [get_object_name [get_attribute $path startpoint]]
                set end [get_object_name [get_attribute $path endpoint]]
                if {[get_attribute [get_attribute $path endpoint] is_clock_gating_pin] == true} {
                    set is_valid [is_clock_gating_valid $end]
                    puts "$slack     $start     $end     $is_valid" 
                    if {!$is_valid && ([lsearch $false_end $end] < 0)} {
                        set false_end [lappend false_end $end]
                    }
                } elseif {[get_attribute [get_attribute $path endpoint] clock_gate_enable_pin] == true} {
                    set is_valid 1 
                    puts "$slack     $start     $end     $is_valid" 
                }
            }
            puts "" 
        } else {
            puts "No violating paths available for checking!"
        }
    }
    unsuppress_message {ATTR-1 ATTR-3 TIM-104}
    puts "\n-----\nSummary of false endpoints:"
    puts $false_end
}

