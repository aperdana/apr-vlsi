####################################################
## Procedure    : export_viol_path
## Author       : aperdana
## Description  : Export all violating timing path into csv format.
####################################################
proc export_viol_path args {
    ## Global Variables and Settings
    global DESIGN MKY
    set NWORST 1
    set CHECK_QUEUE "max min"
    set OPTION(-max_paths) 100
    set OPTION(-prefix) ""
    set OPTION(-dir) "."
    parse_proc_arguments -args $args OPTION
    if {[info exists OPTION(-type)] && ![string match $OPTION(-type) both]} {
        set CHECK_QUEUE ${OPTION(-type)}
    }
    if {$OPTION(-prefix) != ""} {
        set OPTION(-prefix) "${OPTION(-prefix)}."
    }
    suppress_message TIM-104
    
    ## Main
    puts "INFO: Exporting all violating paths for $CHECK_QUEUE check type with maximum number of paths $OPTION(-max_paths) per path group."
    foreach check $CHECK_QUEUE {
        puts "INFO: Starting to export violating paths for $check check"
        set filename "${OPTION(-dir)}/${OPTION(-prefix)}${check}.viol_path.csv"
        if {[file exists $filename]} {
            puts "WARN: Target file $filename exists and will be replaced."
        }
        set outfile [open $filename w]
        puts $outfile "Scenario, Group, Slack, Startpoint, Endpoint, Start Clock, End Clock, Skew, Arrival Time, Required Time"
        if {[string match $check max]} {
            set scenarios [get_scenarios -active true -setup true]
        } else {
            set scenarios [get_scenarios -active true -hold true]
        }
        foreach scen $scenarios {
            current_scenario $scen
            foreach_in_collection group [get_path_groups] {
                set group_name [get_attribute $group name]
                set target_paths_in_group [sort_collection [get_timing_paths -delay_type $check -nworst $NWORST -max_paths $OPTION(-max_paths) -slack_lesser_than 0 -group $group_name  -scen $scen] slack]
                if {[sizeof_collection $target_paths_in_group] > 0} {
                    foreach_in_collection path $target_paths_in_group {
                        set slack [get_attribute $path slack]
                        set start [get_attribute [get_attribute $path startpoint] full_name]
                        set end [get_attribute [get_attribute $path endpoint] full_name]
                        set start_clock [get_attribute [get_attribute $path startpoint_clock] full_name]
                        set start_clock_lat [get_attribute $path startpoint_clock_latency]
                        set end_clock [get_attribute [get_attribute $path endpoint_clock] full_name]
                        set end_clock_lat [get_attribute $path endpoint_clock_latency]
                        set skew [expr $end_clock_lat - $start_clock_lat]
                        set arrival [get_attribute $path arrival]
                        set required [get_attribute $path required]
                        puts $outfile "$scen, $group_name, $slack, $start, $end, $start_clock, $end_clock, $skew, $arrival, $required"
                    }
                }
            }
        }
        puts "INFO: Finished exporting all violating paths for $check check to $filename"
        close $outfile
    }
    unsuppress_message TIM-104
}

define_proc_attributes export_viol_path \
    -info "Export all violating timing paths in all active scenarios into CSV format" \
    -define_args {\
        {"-max_paths" "Maximum number of paths reported per path group. Default is 100" int int optional} \
        {"-type" "Check type. Default is both" "both|max|min" one_of_string {optional {values {both max min}}}} \
        {"-dir" "Output directory. Default is current directory" string string optional} \
        {"-prefix" "Prefix for output file. Dot will be added at the end of prefix" string string optional} \
     }
