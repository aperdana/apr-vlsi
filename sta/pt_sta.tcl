####################################################
## Script       : pt_sta.tcl
## Author       : aperdana
## Description  : Run post#layout STA using PrimeTime
##                Based on script by dcahyadi 
####################################################

source /proj/cadpnr/pds/flow/4.0/flow/proc.pnr/pnrUtils
source /proj/cadpnr/pds/dev/pdt/pdSignoffCriteria.tcl
source /proj/cadpnr/pds/tools/pt/clock/pdReportClockTran_pt.tcl
suppress_message CMD-041

pnrReadConf
mkySetRefLibraries

##--------------------------------------------------
## Control Parameters
## Any duplicate variable defined in pds.conf will be overwritten
## by the same variable below.
## pt_derate_timing
## pt_xtalk_timing 
## netlist_file
## sdc_file
## spef_file
## LOG
## REPORT
##--------------------------------------------------

set pt_derate_timing        true
set pt_xtalk_timing         true
set netlist_file            ./datain/netlist.vg
set sdc_file                ./datain/sdc.sdc
set spef_file               ./datain/spef.spef

##--------------------------------------------------
## Crosstalk Settings
##--------------------------------------------------
if {
