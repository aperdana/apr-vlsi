##--------------------------------------------------
## File         : bindkey.tcl
## Author       : aperdana
## Description  : Contains bindkeys for Cadence Innovus
##                Modified from Jim Keefer's original script
##--------------------------------------------------

## Layer visibility
bindKey Key-0 "setLayerPreference quickmain_Wire&Via -isVisible 1 -isSelectable 1"
bindKey Key-1 "setLayerPreference allM1 -isVisible 1 -isSelectable 1; setLayerPreference allM2Cont -isVisible 1 -isSelectable 1"
bindKey Key-2 "setLayerPreference allM2 -isVisible 1 -isSelectable 1; setLayerPreference allM3Cont -isVisible 1 -isSelectable 1"
bindKey Key-3 "setLayerPreference allM3 -isVisible 1 -isSelectable 1; setLayerPreference allM4Cont -isVisible 1 -isSelectable 1"
bindKey Key-4 "setLayerPreference allM4 -isVisible 1 -isSelectable 1; setLayerPreference allM5Cont -isVisible 1 -isSelectable 1"
bindKey Key-5 "setLayerPreference allM5 -isVisible 1 -isSelectable 1; setLayerPreference allM6Cont -isVisible 1 -isSelectable 1"
bindKey Key-6 "setLayerPreference allM6 -isVisible 1 -isSelectable 1; setLayerPreference allM7Cont -isVisible 1 -isSelectable 1"
bindKey Key-7 "setLayerPreference allM7 -isVisible 1 -isSelectable 1; setLayerPreference allM8Cont -isVisible 1 -isSelectable 1"
bindKey Key-8 "setLayerPreference allM8 -isVisible 1 -isSelectable 1;"

bindKey Ctrl+0 "setLayerPreference quickmain_Wire&Via -isVisible 0 -isSelectable 0"
bindKey Ctrl+1 "setLayerPreference allM1 -isVisible 0 -isSelectable 0; setLayerPreference allM2Cont -isVisible 0 -isSelectable 0"
bindKey Ctrl+2 "setLayerPreference allM2 -isVisible 0 -isSelectable 0; setLayerPreference allM3Cont -isVisible 0 -isSelectable 0"
bindKey Ctrl+3 "setLayerPreference allM3 -isVisible 0 -isSelectable 0; setLayerPreference allM4Cont -isVisible 0 -isSelectable 0"
bindKey Ctrl+4 "setLayerPreference allM4 -isVisible 0 -isSelectable 0; setLayerPreference allM5Cont -isVisible 0 -isSelectable 0"
bindKey Ctrl+5 "setLayerPreference allM5 -isVisible 0 -isSelectable 0; setLayerPreference allM6Cont -isVisible 0 -isSelectable 0"
bindKey Ctrl+6 "setLayerPreference allM6 -isVisible 0 -isSelectable 0; setLayerPreference allM7Cont -isVisible 0 -isSelectable 0"
bindKey Ctrl+7 "setLayerPreference allM7 -isVisible 0 -isSelectable 0; setLayerPreference allM8Cont -isVisible 0 -isSelectable 0"
bindKey Ctrl+8 "setLayerPreference allM8 -isVisible 0 -isSelectable 0;"

## Display mode
proc encSetDisplayMode {mode} {
  setDrawView place
  setLayerPreference  page2_1 -isVisible 0 -isSelectable 0
  setLayerPreference  page2_2 -isVisible 1 -isSelectable 1
  setLayerPreference text -isVisible 1  -isSelectable 1
  switch -regexp $mode {
    "placement" {
      setLayerPreference hinst -isVisible 1 -isSelectable 1
      setLayerPreference inst -isVisible 1 -isSelectable 1
      setLayerPreference block -isVisible 1 -isSelectable 1
      if {$mode=="placement_hm"} {
        puts "DisplayMode:placement (HM only)..."
        setLayerPreference metalFill -isVisible 0 -isSelectable 0
        setLayerPreference net -isVisible 0
        setLayerPreference pg -isVisible 0
        setLayerPreference power -isVisible 0
        setLayerPreference stdCell -isVisible 0 -isSelectable 0 ;
        setLayerPreference phyCell -isVisible 1 -isSelectable 1 ;
        setLayerPreference coverCell -isVisible 1 -isSelectable 1 ;
        setLayerPreference block -isVisible 1 -isSelectable 1 ;
        setLayerPreference layerBlk -isVisible 1 -isSelectable 1 ;
        setLayerPreference obstruct -isVisible 1 -isSelectable 1 ;
        setLayerPreference screen -isVisible 1 -isSelectable 1 ;
        setLayerPreference macroOnly -isVisible 1 -isSelectable 1 ;
        setLayerPreference layerBlk -isVisible 1 -isSelectable 1 ;
        setLayerPreference blockHalo -isVisible 1 -isSelectable 1 ;
        setLayerPreference routingHalo -isVisible 1 ;
        setLayerPreference blkLink -isVisible 1 ;
        setLayerPreference cellBlkgObj -isVisible 1 ;
      } else {
        puts "DisplayMode:placement (HM & std)..."	
        setLayerPreference metalFill -isVisible 0 -isSelectable 0
        setLayerPreference net -isVisible 0
        setLayerPreference pg -isVisible 0
        setLayerPreference power -isVisible 0
        setLayerPreference stdCell -isVisible 1 -isSelectable 1 ;
        setLayerPreference phyCell -isVisible 1 -isSelectable 1 ;
        setLayerPreference coverCell -isVisible 1 -isSelectable 1 ;
        setLayerPreference block -isVisible 1 -isSelectable 1 ;
        setLayerPreference layerBlk -isVisible 1 -isSelectable 1 ;
        setLayerPreference obstruct -isVisible 1 -isSelectable 1 ;
        setLayerPreference screen -isVisible 1 -isSelectable 1 ;
        setLayerPreference macroOnly -isVisible 1 -isSelectable 1 ;
        setLayerPreference layerBlk -isVisible 1 -isSelectable 1 ;
        setLayerPreference blockHalo -isVisible 1 ;
        setLayerPreference routingHalo -isVisible 1 ;
        setLayerPreference blkLink -isVisible 0 ;
        setLayerPreference cellBlkgObj -isVisible 1 ;
      }
      puts "DisplayMode:place-done"
    }
    "route" {
      switch -glob $mode {
        "preroute" { 
          puts "DisplayMode:preroute (w/o std cell rail)..."
          setLayerPreference stdCell -isVisible 0 -isSelectable 0 ;
          setLayerPreference metalFill -isVisible 1 -isSelectable 1
          setLayerPreference net -isVisible 0
          setLayerPreference pg -isVisible 1
          setLayerPreference power -isVisible 1
          setLayerPreference allM0 -isVisible 0 -isSelectable 0
          setLayerPreference allM1Cont -isVisible 0 -isSelectable 0
          setLayerPreference allM1 -isVisible 0 -isSelectable 0
          setLayerPreference allM2Cont -isVisible 0 -isSelectable 0
          setLayerPreference allM2 -isVisible 1 -isSelectable 1
          setLayerPreference allM3Cont -isVisible 1 -isSelectable 1
          setLayerPreference allM3 -isVisible 1 -isSelectable 1
          setLayerPreference allM4Cont -isVisible 1 -isSelectable 1
          setLayerPreference allM4 -isVisible 1 -isSelectable 1
          setLayerPreference allM5Cont -isVisible 1 -isSelectable 1
          setLayerPreference allM5 -isVisible 1 -isSelectable 1
          setLayerPreference allM6Cont -isVisible 1 -isSelectable 1
          setLayerPreference allM6 -isVisible 1 -isSelectable 1
          setLayerPreference allM7Cont -isVisible 1 -isSelectable 1
          setLayerPreference allM7 -isVisible 1 -isSelectable 1
          setLayerPreference allM8Cont -isVisible 1 -isSelectable 1
          setLayerPreference allM8 -isVisible 1 -isSelectable 1
          setLayerPreference obstruct -isVisible 0 -isSelectable 0
          setLayerPreference screen -isVisible 0 -isSelectable 0
          setLayerPreference macroOnly -isVisible 0 -isSelectable 0
          setLayerPreference layerBlk -isVisible 0 -isSelectable 0
          setLayerPreference blockHalo -isVisible 0
          setLayerPreference routingHalo -isVisible 0
          setLayerPreference blkLink -isVisible 0
        }
        "preroute_PinConStd" { 
          puts "DisplayMode:preroute (w std cell rail)..."
          setLayerPreference stdCell -isVisible 1 -isSelectable 1 ;
          setLayerPreference metalFill -isVisible 1 -isSelectable 1
          setLayerPreference net -isVisible 0
          setLayerPreference pg -isVisible 1
          setLayerPreference power -isVisible 1
          setLayerPreference allM0 -isVisible 1 -isSelectable 1
          setLayerPreference allM1Cont -isVisible 1 -isSelectable 1
          setLayerPreference allM1 -isVisible 1 -isSelectable 1
          setLayerPreference allM2Cont -isVisible 1 -isSelectable 1
          setLayerPreference allM2 -isVisible 1 -isSelectable 1
          setLayerPreference allM3Cont -isVisible 1 -isSelectable 1
          setLayerPreference allM3 -isVisible 1 -isSelectable 1
          setLayerPreference allM4Cont -isVisible 1 -isSelectable 1
          setLayerPreference allM4 -isVisible 1 -isSelectable 1
          setLayerPreference allM5Cont -isVisible 1 -isSelectable 1
          setLayerPreference allM5 -isVisible 1 -isSelectable 1
          setLayerPreference allM6Cont -isVisible 1 -isSelectable 1
          setLayerPreference allM6 -isVisible 1 -isSelectable 1
          setLayerPreference allM7Cont -isVisible 1 -isSelectable 1
          setLayerPreference allM7 -isVisible 1 -isSelectable 1
          setLayerPreference allM8Cont -isVisible 1 -isSelectable 1
          setLayerPreference allM8 -isVisible 1 -isSelectable 1
          setLayerPreference obstruct -isVisible 0 -isSelectable 0
          setLayerPreference screen -isVisible 0 -isSelectable 0
          setLayerPreference macroOnly -isVisible 0 -isSelectable 0
          setLayerPreference layerBlk -isVisible 0 -isSelectable 0
          setLayerPreference blockHalo -isVisible 0
          setLayerPreference routingHalo -isVisible 0
          setLayerPreference blkLink -isVisible 0
        }
        "droute_0" {
          puts "DisplayMode:droute_0 (w/o HM pins)..."
          setLayerPreference net -isVisible 1 -isSelectable 1
        }
        "droute_1" {
          puts "DisplayMode:droute_1 (w HM pins)..."
          setLayerPreference net -isVisible 1 -isSelectable 1
          setLayerPreference pinObj -isVisible 1 -isSelectable 1
        }
      }
      puts "DisplayMode:route-done"
    }     
  }      
}
bindKey F4 "encSetDisplayMode placement_hm"
bindkey F5 "encSetDisplayMode placement"
bindKey F6 "encSetDisplayMode preroute"
bindKey F9 "encSetDisplayMode preroute_PinConStd"
bindKey F10 "encSetDisplayMode droute_0"
bindKey F11 "encSetDisplayMode droute_1"

## Ruler   
bindKey k "uiSetTool ruler"
bindKey Ctrl+k "cleanRuler"
bindKey Shift+k "cleanRuler"

## Set layer display
bindkey Alt+m "encSetLayerDisplay"

## Zoom
bindKey z   "zoomIn"
bindKey Ctrl+z "zoomIn"
bindKey Shift+z "zoomOut"
bindKey Alt+z   "zoomSelected"

## Fit
bindKey f   "fit"
bindKey Alt+f   "Previous"

## undo/redo
bindKey u "undo"
bindKey Ctrl+u "redo"
bindKey Shift+u "redo"

## hightlight
bindKey h "highlightExternalNets"

## select
bindKey Ctrl+d "deselectAll"

## Help message
set CONFIG_HELP(bindkey) " \
     \nPNR-INFO:BINDKEY --- Layer Display Modes -------------------------\
     \n\ttoggle 1-9:  turn on/off metaln and vian. \
     \n\t0/ctrl1+0:  turn on/off all metal/via layers \
     \n\
     \nPNR-INFO:BINDKEY ---  Layout Display Modes -------------------------\
     \n\tF4:  floorplan mode (HM only) \
     \n\tF5:  placement mode (HM and stdcell) \
     \n\tF6:  preroute (w/o stdcell pin) \
     \n\tF9:  preroute (w/ stdcell pin) \
     \n\tF10: droute mode 0 (w/o HM pins) \
     \n\tF11: droute mode 2 (w/ HM pins) \
     \n\
     \nPNR-INFO:BINDKEY ---- Common tasks ---------------------------\
     \n\tSpace:             toggle on/off console window \
     \n\tCtrl+Space:        toggle on/off view setting window \
     \n\tu | ctrl+u:        undo | redo \
     \n\tk | ctrl+k:        ruler tool | remove ruler \
     \n\tz | ctrl+z | shift+z | alt+z: zoom mode | zoom out | zoom in | zoom and fit selected \
     \n\tf | ctrl+f | shift+f:  fit all | fit selected / previous view \
     \n\th | ctrl+h | shift+h:  hightlight selected | clear all highlights | highlight net of selected opjects \
     \n\ta | ctrl+a | shift+a:  select by name | clear selected | select nets of selected objects \
     \n\tx | ctrl+x | shift+x:  open selected cell's FRAM | CELL | CELL view \
     \n\tRest of bindkeys are Cadence defaults."

