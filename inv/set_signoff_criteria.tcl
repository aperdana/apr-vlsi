####################################################
## Procedure    : set_signoff_criteria 
## Author       : aperdana
## Description  : Set timing derate for specified delay corners based on pdSignoffCriteria. 
####################################################
proc set_signoff_criteria {args} {
    global env
    set supported_proc "28lp"
    ##--------------------------------------------------
    ## Validates Options
    ##--------------------------------------------------
    parse_proc_arguments -args $args opt
    # Check validity of all delay corners specified in option
    # Exit if one of them is not valid
    foreach dc $opt(-dc) {
        if {[lsearch [all_delay_corners] $dc] == -1} {
            puts "ERROR: Delay corner $dc does not exist"
            return 1
        }
    }
    # Check if process option specified
    # If no process option specified, use PNR_TECHNOLOGY as reference
    if {[info exists opt(-p)]} {
        # Check if process supported and set global variable "process" 
        # Exit if process is not supported
        if {[lsearch $supported_proc $opt(-p)] > -1} {
            set p $opt(-p)
            puts "INFO: Setting signoff criteria for $p process"
        } else {
            puts "ERROR: Process $opt(-p) not supported"
            return 1
        }
    } else {
        # Check if env. variable PNR_TECHNOLOGY is set
        # Exit if it is not and ask user to either set it manually or use -p
        if {[info exists env(PNR_TECHNOLOGY)]} {
            switch -glob $env(PNR_TECHNOLOGY) {
                *28lp* { set p 28lp }
                *28rf* { set p 28lp }
                default { set p "" }
            }
            puts "INFO: Setting signoff criteria for $p process"
        } else {
            puts "ERROR: PNR_TECHNOLOGY is not set. Either set it manually or use -p to specify process"
            return 1
        }
    }
    ##--------------------------------------------------
    ## Signoff Criteria
    ##--------------------------------------------------
    switch $p {
        ##-------------------------------------
        ## 28LP Signoff Criteria:
        ##-------------------------------------
        ## DEFAULT -- Newly proposed criteria:
        ##  - Applied to both cell/net delays. 
        ##  - setup: 30ps uncertainty. xtalk required.
        ##  - hold_slow: -16% on launch/data. 45ps uncertainty. xtalk required.
        ##  - hold_typ:  +/-8% on capture/data/launch. 30ps uncertainty. xtalk required.
        ##  - hold_fast: +16% on capture. 15ps uncertainty. xtalk required.
        ## -aocv -- use AOCV table on clock network but DEFAULT constrant OCV on data. 
        ## -old -- use previous proposed criteria:
        ##  - Applied to both clock/data and cell/net delays. 
        ##  - setup: xtalk recommended, 30ps uncertainty
        ##  - hold_slow: +/-7% on capture/data/launch. 45ps uncertainty. xtalk required.
        ##  - hold_typ:  +/-7% on capture/data/launch. 30ps uncertainty. xtalk required.
        ##  - hold_fast: +/-7% on capture/data/launch. 15ps uncertainty. xtalk required.
        ##  - (*dly*|*locvx*: additional 20%|13%)
        ## Delay cell has special hold derating factor:
        ##  - *dly*|*locvx*: additional +/- 13%|6% (relative to clock derate)
        ##-------------------------------------
        28lp {
            set DERATE(default)       0.07
            set DERATE(slow_launch)   0.16
            set DERATE(fast_capture)  0.16
            set DERATE(typ_both)      0.08
            set DERATE(setup_typ)     0.10
            set DERATE(dly)           0.13
            set DERATE(locvx)         0.06
        }
        default {}
    }
    ##--------------------------------------------------
    ## Apply Signoff Criteria 
    ##--------------------------------------------------
    foreach dc $opt(-dc) {
        puts "INFO: Applying signoff criteria on delay corner $dc"
        switch $p {
            28lp {
                if {[sizeof_collection [set cells [get_lib_cells -regexp {.*/.*dly\d.*}]] > 0]} {
                    eval pnrCmd set_timing_derate -delay_corner $dc -cell_delay -late [expr 1 + $DERATE(dly)] $cells
                    eval pnrCmd set_timing_derate -delay_corner $dc -cell_delay -early [expr 1 - $DERATE(dly)] $cells
                }
                if {[sizeof_collection [set cells [get_lib_cells -regexp {.*/.*dlylocv.*}]] > 0]} {
                    eval pnrCmd set_timing_derate -delay_corner $dc -cell_delay -late [expr 1 + $DERATE(locvx)] $cells
                    eval pnrCmd set_timing_derate -delay_corner $dc -cell_delay -early [expr 1 - $DERATE(locvx)] $cells
                }

                if {[regexp {slow.*hold} $dc]} {
                    eval pnrCmd set_timing_derate -delay_corner $dc -early [expr 1 - $DERATE(slow_launch)]
                }
                if {[regexp {typ.*hold} $dc]} {
                    eval pnrCmd set_timing_derate -delay_corner $dc -early [expr 1 - $DERATE(typ_both)]
                    eval pnrCmd set_timing_derate -delay_corner $dc -late [expr 1 + $DERATE(typ_both)]
                }
                if {[regexp {fast.*hold} $dc]} {
                    eval pnrCmd set_timing_derate -delay_corner $dc -late [expr 1 + $DERATE(fast_capture)]
                }
            }
        }
    }
    puts "INFO: Done applying signoff criteria"
    return 0
}

define_proc_arguments set_signoff_criteria \
    -info "Set timing derate for specified delay corners based on pdSignoffCriteria. The command takes PNR_TECHNOLOGY environment variable as reference for uncertainty and derate value.\nSupported process: 28lp" \
    -define_args [list \
        [list -dc \
            "Delay corner to be applied" \
            "list_of_dc" \
            "list" \
            "required" \
        ] \
        [list -p \
            "Overrides PNR_TECHNOLOGY. Specify one of this: 28lp" \
            "proc_name" \
            "string" \
            "optional" \
        ] \
    ]
