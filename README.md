# apr-vlsi #

apr-vlsi is a collection of shell and native EDA scripts for VLSI.

## Minimum Requirements ##
Shell scripts:

* Perl 5.10
* Tcl 8.4

EDA scripts:

* Synopsys IC Compiler K-2015
* Cadence Innovus 15.21

## Usage ##

All scripts are readily usable. For EDA scripts, you just need to source all of them in your EDA tool shell and it will be available for use. For shell scripts, you can directly invoke them in your shell.

## Directory Structure ##
Shell scripts are located in `etc/`, while EDA scripts are organized based on the tool. Dependencies are located in `lib/`.

Below is the complete directory structure.

`apr-vlsi`
> `etc`
> > *Shell scripts*
>
> `icc`
> > *IC Compiler scripts*
>
> `inv`
> > *Innovus scripts*
>
> `lib`
> > `perl`
> > > *Perl libraries*
>
> `sta`
> > *Static Timing Analysis scripts*

## Contact ###
Please send email to [ant.perdana@gmail.com](mailto:ant.perdana@gmail.com) for questions and suggestions