#!/bin/csh

set verilog="../dataout/RCDI.RCDI.eco_place_route.vg"
set gds_asm="$PV_INT_ROOT/RCDI/1.3.3/dataout/gds/RCDI.gds.asm"
set mtt="6"
set top="RCDI"
if (-e $verilog && -e $gds_asm) then
    /proj/libs/marvell/tsmc40lp/svt/m9szd_tielib/rev4.0.1/bin/genXref.pl -i $verilog -t $top
    /proj/libs/marvell/tsmc40lp/svt/m9szd_tielib/rev4.0.1/bin/tieComp -mtt $mtt -xref $top.xref
    /proj/libs/marvell/tsmc40lp/svt/m9szd_tielib/rev4.0.1/bin/gdsRepTieCell.pl -i $gds_asm -m TieCellComp.map -t $mtt -o $top.tielib.gds
else
    echo "File(s) do(es) not exist(s)"
endif
