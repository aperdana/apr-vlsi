#!/usr/bin/perl

####################################################
## Script       : summary_log 
## Author       : aperdana
## Description  : Summarize warnings and errors in log file
##                Default log file type is Innovus
####################################################

use 5.010;
use strict;
use File::Basename 'dirname';
use lib "${\(dirname $0)}/../libs/perl";
use Text::Table;
use Getopt::Std;
use Switch;

##--------------------------------------------------
## GENERAL GLOBAL VARIABLES / CONSTANTS
##--------------------------------------------------
## Constants
my $VERSION = 2.3;                                          # Script version
$Getopt::Std::STANDARD_HELP_VERSION = 1;                    # Enable standard help version
my $MAX_CHAR = 85;                                          # Max. num. of characters stored for warning/error message
my $MAX_LINE = 4;                                           # Max. num. of line for multi-line warning/error message
my $WIDTH = 111;                                            # Display width 

## Variables
my @logs;                                                   # List of log files to be summarized
my %error;                                                  # Errors hash table
my %warning;                                                # Warnings hash table
my $type = 'inv';                                           # Log type
my %option;                                                 # Script options
my %keyword = (                                             # Keyword for warning and error lookup
    inv => {
        warning => "WARN",
        error => "ERROR"
    },
    icc => {
        warning => "Warning",
        error => "Error"
    }
);
my %single_pattern = (                                      # Pattern for single-line warning and error lookup
    inv => qr/\*\*(?<keyword>$keyword{inv}{warning}|$keyword{inv}{error}):\s\((?<code>([A-Z]|-|\d)+?)\):\s+(?<mess>.+?)$/, 
    icc => qr/^(?<keyword>$keyword{icc}{warning}|$keyword{icc}{error}):\s(?<mess>.+?)\s\((?<code>([A-Z]|-|\d)+?)\)$/
);
my %multi_pattern = (                                       # Pattern to start/stop multi-line warning/error lookup
    icc => {
        start => qr/^(?<keyword>$keyword{icc}{warning}|$keyword{icc}{error}):\s(?<mess>.+)$/,
        stop => qr/^(?<mess>.+?)\s\((?<code>([A-Z]|-|\d)+?)\)$/
    },
);
my $sep = " ";                                              # Output table separator

##--------------------------------------------------
## SUB-ROUTINE PROTOTYPES
##--------------------------------------------------
# Sub  : store
# Arg  : is_added (reference to a hash)
#        log (scalar)
#        keyword (scalar)
#        code (scalar)
#        message (scalar)
# Ret  : is_added (hash)
# Des  : Clip error/warning message if number of char is greater than $MAX_CHAR and add it to appropriate output hash based on keyword
#        only if keyword of the same code hasn't been added yet. If it has been added, only increment counter in output hash.
sub store;

##--------------------------------------------------
## OPTIONS VALIDATION
## Options available:
##     -t <inv|icc>       Log type, can be 'inv' (Innovus) or 'icc' (IC Compiler).
##                        Default to inv.
## Exit code: 1
##--------------------------------------------------
# Get script options
# Exit if invalid options found
if (not defined @ARGV) {
    &HELP_MESSAGE;
    exit 1;
}
if (!getopts('t:', \%option)) {
    #say "ERROR: Invalid options, use --help for more info";
    exit 1;
}
# Check validity of log type option
# Print error if type is not supported
# and exit
if ($option{t}) {
    if (grep {$_ eq $option{t}} keys %single_pattern) {
        $type = $option{t};
    } else {
        say "ERROR: Type \"$option{t}\" is not supported.";
        exit 1;
    }
}
# Check accessibility of each log file 
# Print error if log files can't be accessed
# and exit
@logs = @ARGV;
my $exit_flag = 0;
foreach my $log (@logs) {
    if (not -e $log) {
        $exit_flag = 1 if not $exit_flag;
        say "ERROR: File $log does not exist.";
    }
}
if ($exit_flag) {
    exit 1;
}

##--------------------------------------------------
## PROCESS LOG FILES 
##--------------------------------------------------
foreach my $log (@logs) {
    my %is_added;                   # Marker for warning/error codes which have been added
    my $mode = "single";            # Mode of operation: "single" or "multi" for single or multi line lookup respectively
    my $counter = 0;                # Count num of lines stored in "multi" mode
    my $temp_mess;                  # Temp. var for storing message in "multi" mode
    my $temp_key;                   # Temp. var for storing keyword in "multi" mode
    my $temp_code;                  # Temp. var for storing code in "multi" mode
    open (LOG, "< $log");
    while (my $line = <LOG>) {
        chomp ($line);
        switch ($mode) {
            case "single" {
                if ($line =~ /$single_pattern{$type}/) {
                    # Single-line pattern detected
                    # Store message
                    # Stay in single mode.
                    %is_added = &store (\%is_added, $log, $+{keyword}, $+{code}, $+{mess});
                } elsif ($line =~ /$multi_pattern{$type}{start}/) {
                    # Multi-line start pattern detected
                    # Store message
                    # Increase counter
                    # Change to multi mode
                    $temp_mess = $+{mess};
                    $temp_key = $+{keyword};
                    $mode = "multi";
                    $counter = 1;
                } else {
                    # Normal log entry, do nothing
                }
            }
            case "multi" {
                if ($line =~ /$single_pattern{$type}/) {
                    # Single pattern detected
                    # Current entry is most likely false-positive
                    # Change to single mode with counter reset to 0
                    %is_added = &store (\%is_added, $log, $+{keyword}, $+{code}, $+{mess});
                    $mode = "single";
                    $counter = 0;
                } elsif ($line =~ /$multi_pattern{$type}{start}/) {
                    # New multi-line pattern detected
                    # Current entry is most-likely false-positive
                    # Stay in multi mode, reinitialize counter to 1, discard temp
                    $temp_mess = $+{mess};
                    $temp_key = $+{keyword};
                    $counter = 1;
                } elsif ($line =~ /$multi_pattern{$type}{stop}/) {
                    # Multi-line stop pattern detected
                    # Append and store message if still within the limit of
                    # max. number of line in multi-line message. Discard if not.
                    # Change to single mode, clear counter
                    if ($counter <= $MAX_LINE) {
                        my $temp_code = $+{code};
                        my $line_cleared = $+{mess};
                        ($line_cleared) = $line_cleared =~ /^\s*?(\S.+)$/;
                        $temp_mess = $temp_mess . ' ' . $line_cleared;
                        %is_added = &store (\%is_added, $log, $temp_key, $temp_code, $temp_mess);
                    }
                    $mode = "single";
                    $counter = 0;
                } else {
                    # Check if current multi-line parsing is still within the limit of
                    # maximum number of line in multi-line message
                    # If not, discard current progress
                    if ($counter < $MAX_LINE) { 
                        # Assume the line is part of multi-line message
                        # Clear any leading whitespace of current line
                        # Append message, increase counter
                        (my $line_cleared) = $line =~ /^\s+?(\S.+)/;
                        $temp_mess = $temp_mess . ' ' . $line_cleared;
                        $counter++;
                    } else {
                        # Exceed max allowed num of line in multi-line message
                        # Discard current progress
                        # Change to single mode, clear counter
                        $mode = "single";
                        $counter = 0;
                    }
                }
            }
            else {}
        }
    }
    close LOG;
}

##--------------------------------------------------
## OUTPUT 
##--------------------------------------------------
foreach my $log (@logs) {
    my $table_error = Text::Table->new("Code",$sep, "Count", $sep, "Message"); # Table for errors 
    my $table_warning = Text::Table->new("Code", $sep, "Count", $sep, "Message"); # Table for warnings
    my $title =  " LOG SUMMARY FOR $log ";
    my $remains = $WIDTH - length ($title);
    if (scalar (keys %{$error{$log}})) {
        foreach my $code (sort (keys %{$error{$log}})) {
            $table_error->load([$code, $sep, $error{$log}{$code}{num}, $sep, $error{$log}{$code}{mess}]);
        }
    } else {
        $table_error->load(["-", $sep, "-", $sep, "-"]);
    }
    if (scalar (keys %{$warning{$log}})) {
        foreach my $code (sort (keys %{$warning{$log}})) {
            $table_warning->load([$code, $sep, $warning{$log}{$code}{num}, $sep, $warning{$log}{$code}{mess}]);
        }
    } else {
        $table_warning-> load(["-", $sep, "-", $sep, "-"]);
    }
    say "\n" . "=" x ($remains / 2 + $remains % 2) . $title . "=" x ($remains / 2); 
    say "Errors Summary:";
    print $table_error;
    say "\nWarnings Summary:";
    print $table_warning;
}
say "=" x $WIDTH;

##--------------------------------------------------
## SUB-ROUTINE
##--------------------------------------------------
# Sub  : store
# Arg  : is_added (reference to a hash)
#        log (scalar)
#        keyword (scalar)
#        code (scalar)
#        message (scalar)
# Ret  : is_added (hash)
# Des  : Clip error/warning message if number of char is greater than $MAX_CHAR and add it to appropriate output hash based on keyword
#        only if keyword of the same code hasn't been added yet. If it has been added, only increment counter in output hash.
sub store {
    my %is_added = %{$_[0]};
    my $log = $_[1];
    my $key = $_[2];
    my $code = $_[3];
    my $message = $_[4];
    if ($is_added{$key}{$code}) {
        if ($key eq $keyword{$type}{warning}) {
            $warning{$log}{$code}{num}++;
        } else {
            $error{$log}{$code}{num}++;
        }
    } else {
        if (length $message > $MAX_CHAR) {
            $message = substr ($message, 0, $MAX_CHAR) . "...";
        }
        if ($key eq $keyword{$type}{warning}) {
            $is_added{$key}{$code} = 1;
            $warning{$log}{$code}{num} = 1;
            $warning{$log}{$code}{mess} = $message;
        } else {
            $is_added{$key}{$code}= 1;
            $error{$log}{$code}{num} = 1;
            $error{$log}{$code}{mess} = $message;
        }
    }
    return %is_added;
}

# Sub  : HELP_MESSAGE
# Arg  : -
# Ret  : - 
# Des  : Display help message. Required by Getopt::Std
sub HELP_MESSAGE {
    print "
Usage: summary_log [OPTION]... LOG1 LOG2... 
Summarize warnings and errors in log file

    -t <inv|icc>       Log type, can be 'inv' (Innovus) or 'icc' (IC Compiler).
                       Default is inv.

    --help             print this help info
    --version          print current version
    
Please report bugs to aperdana\@marvell.com\n"
}

# Sub  : VERSION_MESSAGE
# Arg  : -
# Ret  : -
# Des  : Display version message. Required by Getopt::Std
sub VERSION_MESSAGE {
    print "summary_log version $VERSION Written by Antonius Perdana Renardy\n";
}

